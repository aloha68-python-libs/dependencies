
class ServiceWithoutParams:
    pass


class ServiceWithNestedService:
    def __init__(self, service: ServiceWithoutParams):
        self.service = service


lambda_param_a = 42
lambda_param_b = "awasagaga"


class ServiceWithDefaultParams:
    def __init__(self, a: int = lambda_param_a, b: str = lambda_param_b):
        self.a = a
        self.b = b


class ServiceWithParams:
    def __init__(self, a: int, b: str):
        self.a = a
        self.b = b


class ServiceWithUntypedParams:
    def __init__(self, a):
        self.a = a


class ServiceWithEverything:
    def __init__(self,
                 a: int,
                 service: ServiceWithoutParams,
                 nested_service: ServiceWithNestedService,
                 b: str = lambda_param_b,
                 ):
        self.a = a
        self.b = b
        self.service = service
        self.nested_service = nested_service
