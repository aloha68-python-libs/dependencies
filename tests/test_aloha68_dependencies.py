import sys

import pytest
import unittest

from aloha68_config import ConfigNode

from aloha68_dependencies import ServiceCollection, DependencyException
from .dependencies_testcase import *


def create_new_service_collection():
    """ Créer une nouvelle collection de services """
    ServiceCollection().reset()
    return ServiceCollection()


class TestDependencies(unittest.TestCase):

    def test_empty_collection(self):
        sc = create_new_service_collection()
        self.assertEqual(len(sc), 0)

    def test_add_transient(self):
        """ Ajout d'un transient """

        sc = create_new_service_collection()
        sc.add_transient(ServiceWithoutParams)
        self.assertEqual(len(sc), 1)

        service = sc.resolve(ServiceWithoutParams)
        self.assertIsInstance(service, ServiceWithoutParams)

        service2 = sc.resolve(ServiceWithoutParams)
        self.assertIsNot(service, service2)

    def test_add_singleton(self):
        """ Ajout d'un singleton """

        sc = create_new_service_collection()
        sc.add_singleton(ServiceWithoutParams)
        self.assertEqual(len(sc), 1)

        service = sc.resolve(ServiceWithoutParams)
        self.assertIsInstance(service, ServiceWithoutParams)

        service2 = sc.resolve(ServiceWithoutParams)
        self.assertIs(service, service2)

    def test_add_instance(self):
        """ Ajout d'une instance """

        service = ServiceWithoutParams()
        sc = create_new_service_collection()
        sc.add_instance(ServiceWithoutParams, service)
        self.assertEqual(len(sc), 1)

        service2 = sc.resolve(ServiceWithoutParams)
        self.assertIs(service, service2)

    def test_add_existing_service(self):
        """ Ajout d'un service existant """

        sc = create_new_service_collection()
        sc.add_transient(ServiceWithoutParams)
        with self.assertRaises(DependencyException):
            sc.add_transient(ServiceWithoutParams)

    def test_resolve_service_default_parameters(self):
        """ Teste la résolution d'un service paramétré """

        sc = create_new_service_collection()
        sc.add_transient(ServiceWithDefaultParams)

        service = sc.resolve(ServiceWithDefaultParams)
        self.assertIsInstance(service, ServiceWithDefaultParams)
        self.assertEqual(service.a, lambda_param_a)
        self.assertEqual(service.b, lambda_param_b)

    def test_resolve_service_conf_parameters(self):
        """ Teste la résolution d'un service avec configuration """

        sc = create_new_service_collection()
        sc.add_transient(ServiceWithDefaultParams)

        conf = ConfigNode({"a": 12, "b": "thomas"})
        service = sc.resolve(ServiceWithDefaultParams, conf)
        self.assertIsInstance(service, ServiceWithDefaultParams)
        self.assertEqual(service.a, 12)
        self.assertEqual(service.b, "thomas")

    def test_resolve_nested_service(self):
        """ Teste la résolution de services imbriqués """

        sc = create_new_service_collection()
        sc.add_transient(ServiceWithoutParams)
        sc.add_transient(ServiceWithNestedService)

        service = sc.resolve(ServiceWithNestedService)
        self.assertIsInstance(service, ServiceWithNestedService)
        self.assertIsInstance(service.service, ServiceWithoutParams)

    def test_resolve_unknown_service(self):
        """ Teste la résolution d'un service inconnu """

        sc = create_new_service_collection()
        with self.assertRaises(DependencyException):
            sc.resolve(ServiceWithoutParams)

    def test_resolve_unprovided_params_service(self):
        """ Teste la résolution d'un service sans paramètres requis """

        sc = create_new_service_collection()
        sc.add_transient(ServiceWithParams)

        with self.assertRaises(DependencyException):
            sc.resolve(ServiceWithParams)

    def test_resolve_untyped_params_service(self):
        """ Teste la résolution d'un service avec un paramètre non typé """

        sc = create_new_service_collection()
        sc.add_transient(ServiceWithUntypedParams)

        with self.assertRaises(NotImplementedError):
            sc.resolve(ServiceWithUntypedParams)

    def test_resolve_positional_only_params_service(self):
        """
        Teste la résolution d'un service avec des paramètres positionnels
        Seulement pour python 3.8 et supérieur
        """

        class PositionalTestedClass:
            def __init__(self, a, /, b):
                pass

        sc = create_new_service_collection()
        sc.add_transient(PositionalTestedClass)

        with self.assertRaises(NotImplementedError):
            sc.resolve(PositionalTestedClass)
