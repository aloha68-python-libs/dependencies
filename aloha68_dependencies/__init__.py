"""
Une collection de services qui permet la mise en place de l'injection de dépendances
"""
from .service_collection import ServiceCollection, DependencyException
