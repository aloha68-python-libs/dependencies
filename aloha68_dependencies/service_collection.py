"""
Représente une collection de services pour permettre l'injection de dépendances
"""

import inspect
import logging
from enum import Enum, auto
from typing import Optional, Type, TypeVar, Union

from aloha68_config import ConfigNode

T = TypeVar("T")
logger = logging.getLogger("aloha68-dependencies")


class DependencyException(Exception):
    """ Une exception de base pour l'injection de dépendances """


class ServiceLifetime(Enum):
    """
    Durée de vie d'un service.

    SINGLETON : une instance est créée lors du premier resolve puis la même
        instance est renvoyée pour tous les autres appels
    TRANSIENT : une nouvelle instance est créée à chaque resolve
    INSTANCE : une unique instance est fournie dès le départ
    """
    SINGLETON = auto()
    TRANSIENT = auto()
    INSTANCE = auto()


class InjectableService:
    """ Représente un service injectable """

    def __init__(self,
                 service_implementation: Union[T, Type[T]],
                 service_lifetime: ServiceLifetime,
                 ):
        """ Initialisation du service """
        self.implementation = service_implementation
        self.lifetime = service_lifetime
        self.instance: Optional[T] = None

        if self.lifetime == ServiceLifetime.INSTANCE:
            self.instance = service_implementation
            self.implementation = type(service_implementation)


class ServiceCollection:
    """ Représente une collection de services """

    __instance = None
    __dependencies: dict[Type[T], InjectableService] = {}

    def __new__(cls):
        """ Méthode spéciale pour gérer le singleton """
        if not ServiceCollection.__instance:
            ServiceCollection.__instance = super().__new__(cls)
            ServiceCollection.__dependencies.clear()
        return ServiceCollection.__instance

    def __len__(self) -> int:
        """ Retourne le nombre de services enregistrés dans la collection """
        return len(self.__dependencies)

    def reset(self):
        """ Retire toutes les dépendances de l'injecteur """
        self.__dependencies.clear()

    def register(self,
                 service_to_add: Type[T],
                 service_implementation: Union[T, Type[T]],
                 service_lifetime: ServiceLifetime,
                 ) -> None:
        """
        La méthode principale pour ajouter des services à la collection.
        :param service_to_add:
            Le service à ajouter à la collection. Ça peut être une classe
            parente pour permettre une implémentation spécifique
        :param service_implementation:
            Le service à implémenter concrètement pour ce type de service
        :param service_lifetime:
            La manière de traiter le service dans le temps
        :return:
        """

        if service_implementation is None:
            service_implementation = service_to_add

        if service_to_add in self.__dependencies:
            logger.error("Ajout d'un service déjà recensé")
            raise DependencyException(
                f"Le service {service_to_add.__name__} a déjà été recensé"
            )

        self.__dependencies[service_to_add] = InjectableService(
            service_implementation=service_implementation,
            service_lifetime=service_lifetime
        )

    def add_singleton(self,
                      service_to_add: Type[T],
                      service_implementation: Optional[Type[T]] = None,
                      ):
        """ Ajoute un service dans la collection en temps que singleton """
        self.register(
            service_to_add, service_implementation, ServiceLifetime.SINGLETON
        )

    def add_transient(self,
                      service_to_add: Type[T],
                      service_implementation: Optional[Type[T]] = None,
                      ):
        """ Ajoute un service dans la collection en temps que transient """
        self.register(
            service_to_add, service_implementation, ServiceLifetime.TRANSIENT
        )

    def add_instance(self, service_to_add: Type[T], service_instance: T):
        """ Ajoute un service dans la collection à partir d'une instance """
        if not isinstance(service_instance, service_to_add):
            raise DependencyException()

        self.register(
            service_to_add, service_instance, ServiceLifetime.INSTANCE
        )

    def _create_instance(self,
                         service: InjectableService,
                         conf: Optional[ConfigNode] = None) -> T:
        """ Créé une instance du service passé en paramètre """

        if not conf:
            conf = ConfigNode()

        kwargs = {}
        service_name = getattr(service.implementation, "__name__", service.implementation)
        signature = inspect.signature(service.implementation.__init__)

        logger.debug(f"Création d'une instance pour le service {service_name}")

        for name, param in signature.parameters.items():

            # On ignore le paramètre self
            if name == "self":
                continue

            # On ignore les paramètres * et **
            if param.kind in [param.VAR_POSITIONAL, param.VAR_KEYWORD]:
                continue

            if param.kind == param.POSITIONAL_ONLY:
                raise NotImplementedError(
                    f"{service_name}.{name}: "
                    f"Nous ne gérons pas les paramètres positionnels"
                )

            if param.annotation is param.empty:
                raise NotImplementedError(
                    f"{service_name}.{name}: "
                    f"Nous ne gérons pas les paramètres non typés"
                )

            # Si le paramètre est un service enregistré dans la collection
            try:
                required_service = self.__dependencies[param.annotation]
            except KeyError:
                pass
            else:
                # On retourne soit l'instance connue, soit la résolution du service
                kwargs[name] = (
                        required_service.instance
                        or self.resolve(required_service.implementation, conf=conf)
                )
                continue

            # Si le paramètre est présent dans la configuration
            if name in conf:
                kwargs[name] = conf[name]
                continue

            # S'il y a une valeur par défaut sur le paramètre
            if param.default is not param.empty:
                kwargs[name] = param.default
                continue

            logger.warning(f"Paramètre impossible à résoudre pour "
                           f"{service_name}: {name}, typé {param.annotation}")

        try:
            return service.implementation(**kwargs)
        except TypeError as ex:
            raise DependencyException(
                f"Impossible de créer une instance de {service_name}: {ex}"
            ) from ex

    def _resolve_singleton(self,
                           service: InjectableService,
                           conf: Optional[ConfigNode] = None) -> T:
        """ Résout un service de type singleton """
        if service.instance is None:
            service.instance = self._create_instance(service, conf)
        return service.instance

    def resolve(self,
                service_type: Type[T],
                conf: Optional[ConfigNode] = None
                ) -> T:
        """ Résout un service afin de récupérer une instance de ce dernier """

        service_name = getattr(service_type, "__name__", service_type)
        logger.debug(f"Récupération d'un service de type {service_name}")

        try:
            service = self.__dependencies[service_type]
        except KeyError as ex:
            raise DependencyException(
                f"Le service {service_name} n'a pas été trouvé dans la collection."
            ) from ex

        if service.lifetime == ServiceLifetime.TRANSIENT:
            return self._create_instance(service, conf)
        if service.lifetime == ServiceLifetime.SINGLETON:
            return self._resolve_singleton(service, conf)
        if service.lifetime == ServiceLifetime.INSTANCE:
            return service.instance

        raise AssertionError("Impossible d'atteindre cette ligne")

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__}(" \
               f"{len(self.__dependencies)} dep(s)" \
               f") at 0x{id(self):x}>"
