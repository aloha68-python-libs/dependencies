# Changelog

Tous les changements notables apportés à ce projet seront documentés dans ce fichier.

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/fr/1.0.0/) et ce projet adhère à la [gestion sémantique de version](https://semver.org/lang/fr/).

## [0.2.0] - 2021-11-23

### Added
- Test permettant de vérifier que les paramètres seulement positionnels sont exclus du projet

### Changed
- Prise en charge de python 3.9 et supérieur (anciennement 3.7 et +)

## [0.1.2] - 2021-11-23

### Added
- Déclenchement d'une exception *NotImplementedError* si un service à résoudre possède un paramètre non typé

### Removed
- Retrait du test qui faisait planter la CI de gitlab. À réintroduire quand on supportera python3.9+

## [0.1.1] - 2021-11-23

### Added
- Utilisation d'un logger nommé *aloha68-dependencies*

### Changed
- La résolution de services transfert la configuration donnée pour les services imbriqués

## [0.1.0] - 2021-11-22

Première version

[0.2.0]: https://gitlab.com/aloha68-python-libs/dependencies/-/tags/0.2.0
[0.1.2]: https://gitlab.com/aloha68-python-libs/dependencies/-/tags/0.1.2
[0.1.1]: https://gitlab.com/aloha68-python-libs/dependencies/-/tags/0.1.1
[0.1.0]: https://gitlab.com/aloha68-python-libs/dependencies/-/tags/0.1.0
