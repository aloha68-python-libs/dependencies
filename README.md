Ce projet permet d'ajouter de l'injection de dépendances à son projet.

## Usage basique

    from aloha68_dependencies import ServiceCollection
    
    class Test:
        pass
    
    collection = ServiceCollection()
    collection.add_transient(Test)
    service = collection.resolve(Test)
    
## Imbrication de services

    class Test:
        pass
    
    class Service:
        def __init__(self, test: Test):
            self.test = test
    
    collection = ServiceCollection()
    collection.add_transient(Test)
    collection.add_transient(Service)
    service = collection.resolve(Service)

## Plus de détails

Méthodes disponibles :

- *add_transient* : ajoute un service qui sera initialisé à chaque appel de *resolve*
- *add_singleton* : ajoute un service qui n'aura qu'une instance unique retournée à chaque appel de *resolve*
- *add_instance* : ajoute une instance qui sera retournée pour chaque appel de *resolve*
- *resolve* : récupère un service enregistré préalablement en essayant de résoudre tous ses paramètres de construction

